from rest_framework_extensions.key_constructor.bits import QueryParamsKeyBit
from rest_framework_extensions.key_constructor.constructors import DefaultKeyConstructor


class PaginatedKeyConstructor(DefaultKeyConstructor):
    limit_offset = QueryParamsKeyBit(params=('limit', 'offset'))


class PurchasesListKeyConstructor(PaginatedKeyConstructor):
    filter = QueryParamsKeyBit(params=('user',))


class ItemsListKeyConstructor(PaginatedKeyConstructor):
    filter = QueryParamsKeyBit(params=('sex', 'price'))
