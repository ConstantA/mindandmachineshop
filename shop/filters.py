import django_filters
import pytz
from django.db import models
from django.utils import timezone

from .models import ShopUser


class ItemFilterSet(django_filters.FilterSet):
    sex = django_filters.ChoiceFilter(field_name='purchases__user__sex', choices=ShopUser.SEX_CHOICES)
    price = django_filters.NumberFilter()

    def filter_queryset(self, queryset):
        queryset = super(ItemFilterSet, self).filter_queryset(queryset)
        shop_user_filter = models.Q(
            purchases__user__date_joined__lte=timezone.datetime(year=2019, month=5, day=1, tzinfo=pytz.UTC)
        )
        sex_filter = self.form.cleaned_data.get('sex')
        if sex_filter:
            shop_user_filter &= models.Q(purchases__user__sex=sex_filter)
        return (
            queryset.annotate(
                        users_count=models.Count(
                            'purchases__user',
                            filter=shop_user_filter,
                            distinct=True
                        )
                    )
                    .order_by('pk', 'name')
        )


class UserIDInFilter(django_filters.BaseInFilter, django_filters.NumberFilter):
    pass


class PurchasesFilterSet(django_filters.FilterSet):
    user = UserIDInFilter(field_name='user', lookup_expr='in', required=True)

    def filter_queryset(self, queryset):
        queryset = super(PurchasesFilterSet, self).filter_queryset(queryset)
        return (
            queryset.values('user')
                    .annotate(
                        total_amount=models.Sum('amount'),
                        total_price=models.Sum(
                            models.ExpressionWrapper(
                                models.F('price') * models.F('amount'),
                                output_field=models.DecimalField()
                            )
                        )
                    )
                    .order_by('user')
                    .values_list('user', 'total_amount', 'total_price', named=True)
        )
