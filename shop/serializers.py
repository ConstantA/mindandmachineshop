from rest_framework import serializers

from .models import Item


class ItemSerializer(serializers.ModelSerializer):
    dttm_created = serializers.DateTimeField(source='created')
    dttm_deleted = serializers.DateTimeField(source='deleted')
    users_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Item
        fields = ('id', 'name', 'details', 'price', 'dttm_created', 'dttm_deleted', 'users_count')


class PurchaseSerializer(serializers.Serializer):
    user = serializers.IntegerField()
    total_amount = serializers.IntegerField()
    total_price = serializers.DecimalField(max_digits=12, decimal_places=2)

    class Meta:
        fields = ('user', 'total_amount', 'total_price')
