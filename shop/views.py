from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework_extensions.cache.mixins import ListCacheResponseMixin

from .cache import ItemsListKeyConstructor, PurchasesListKeyConstructor
from .filters import ItemFilterSet, PurchasesFilterSet
from .models import Item, Purchase
from .serializers import ItemSerializer, PurchaseSerializer


class ItemListAPIView(ListCacheResponseMixin, ListAPIView):
    queryset = Item.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ItemFilterSet
    serializer_class = ItemSerializer
    pagination_class = LimitOffsetPagination
    list_cache_key_func = ItemsListKeyConstructor()


class PurchasesListAPIView(ListCacheResponseMixin, ListAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = PurchasesFilterSet
    pagination_class = LimitOffsetPagination
    list_cache_key_func = PurchasesListKeyConstructor()
