from django.contrib.auth.models import AbstractUser as DjangoAbstractUser, UserManager
from django.db import models
from django.utils import timezone


class RemovableQuerySet(models.QuerySet):
    def remove(self):
        return self.update(deleted=timezone.now())

    def restore(self):
        return self.update(deleted=None)

    def active(self):
        return self.filter(deleted__isnull=True)


RemovableManager = RemovableQuerySet.as_manager()


class RemovableModel(models.Model):
    deleted = models.DateTimeField(blank=True, null=True, db_index=True)

    objects = RemovableManager

    class Meta:
        abstract = True

    def remove(self):
        self.deleted = timezone.now()
        self.save(update_fields=['deleted'])

    def restore(self):
        self.deleted = None
        self.save(update_fields=['deleted'])


class Item(RemovableModel):
    created = models.DateTimeField(default=timezone.now, db_index=True)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    name = models.CharField(max_length=16, default='', blank=True)
    details = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.price)

    class Meta:
        ordering = ('name', 'created', 'pk')


class ShopUser(RemovableModel, DjangoAbstractUser):
    SEX_FEMALE = 'F'
    SEX_MALE = 'M'
    SEX_CHOICES = (
        (SEX_FEMALE, 'Female',),
        (SEX_MALE, 'Male',),
    )
    sex = models.CharField(max_length=1, choices=SEX_CHOICES)
    bought_items = models.ManyToManyField(Item, through='Purchase')

    objects = UserManager.from_queryset(RemovableQuerySet)()
ShopUser._meta.get_field('date_joined').db_index = True  # creating index on date_joined


def get_sentinel_user():
    return ShopUser.objects.get_or_create(username='DELETED', sex=ShopUser.SEX_MALE)[0]


def get_sentinel_item():
    return Item.objects.get_or_create(name='DELETED', price=0)[0]


class Purchase(models.Model):
    user = models.ForeignKey('ShopUser', related_name='purchases', on_delete=models.SET(get_sentinel_user))
    item = models.ForeignKey('Item', related_name='purchases', on_delete=models.SET(get_sentinel_item))
    amount = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=12, decimal_places=2, editable=False)
    created = models.DateTimeField(default=timezone.now, db_index=True)
        
    def save(self, *args, **kwargs):
        if not self.pk and self.item.price:
            self.price = self.item.price
        super(Purchase, self).save(*args, **kwargs)

    class Meta:
        ordering = ('user', 'item', 'created', 'pk')
