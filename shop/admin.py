from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import Item, Purchase, ShopUser


@admin.register(ShopUser)
class ShopUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'sex')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined' , 'deleted')}),
    )
    list_filter = UserAdmin.list_filter + ('sex',)


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'price', 'created', 'deleted')
    list_display_links = ('name', )
    list_editable = ('price',)


@admin.register(Purchase)
class PurchaseAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user', 'item', 'amount', 'price', 'created')
    readonly_fields = ('price',)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(PurchaseAdmin, self).get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ('user', 'item', 'amount', 'price', 'created')
        return readonly_fields

