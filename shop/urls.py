from django.urls import path

from .views import ItemListAPIView, PurchasesListAPIView

app_name = 'shop'

urlpatterns = [
    path('purchases/', PurchasesListAPIView.as_view(), name='purchases'),
    path('items/', ItemListAPIView.as_view(), name='items'),
]
