##MindAndMachineShop

###Requirements:
- docker
- docker-compose

###Init:
```bash
docker-compose up
docker-compose exec web python manage.py migrate
docker-compose exec web python manage.py createsuperuser
```

###Endpoints:
All endpoints are paginated and responses are cached using composite keys, respecting filtering and pagination 


```/shop/items/``` Filter params: `sex` - either 'M' or 'F', `price` - numeric; Pagination params: `limit`, `offset`

```/shop/purchases/``` Filter params: `user` - comma-separated ids; Pagination params: `limit`, `offset`

